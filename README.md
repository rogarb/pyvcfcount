pyvcfcount
====

Description:
------------
A small utility to count the number of entries in a .vcf file.

Installation:
-------------
Copy the python script somewhere in the `PATH` or use installation script `install.py`.
See `./install.py --help`.


Usage:
------
See `pyvcfcount.py --help`.
