#!/usr/bin/env python3

import argparse

parser = argparse.ArgumentParser(
    description="A small utility to count the number of entries in a .vcf file"
)
parser.add_argument("file", help="The .vcf file to parse")
parser.add_argument(
    "--mincount",
    "-m",
    required=False,
    type=int,
    help="the optional minimal count of elements in the VCARD to print on screen",
)

args = parser.parse_args()

table = []

with open(args.file, "r") as f:
    linenum = 0
    count = 0
    current_linenum = 0

    while True:
        line = f.readline()
        # print("read line: ", line)
        linenum += 1
        if line == "":
            break
        elif line == "BEGIN:VCARD\n":
            current_linenum = linenum
            count = 0
        elif line == "END:VCARD\n":
            table.append({"line number": current_linenum, "count": count})
        else:
            count += 1

if args.mincount is not None:
    table = list(filter(lambda el: el["count"] >= args.mincount, table))

for entry in table:
    print("line:", entry["line number"], "/ entries:", entry["count"])
